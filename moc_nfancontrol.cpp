/****************************************************************************
** Meta object code from reading C++ file 'nfancontrol.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../nfancontrol/nfancontrol.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'nfancontrol.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_nfancontrol_t {
    QByteArrayData data[11];
    char stringdata0[254];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_nfancontrol_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_nfancontrol_t qt_meta_stringdata_nfancontrol = {
    {
QT_MOC_LITERAL(0, 0, 11), // "nfancontrol"
QT_MOC_LITERAL(1, 12, 34), // "on_horizontalSlider_sliderRel..."
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 26), // "on_spinBox_editingFinished"
QT_MOC_LITERAL(4, 75, 33), // "on_actionGnome_Terminal_trigg..."
QT_MOC_LITERAL(5, 109, 29), // "on_actionTerminator_triggered"
QT_MOC_LITERAL(6, 139, 26), // "on_actionKonsole_triggered"
QT_MOC_LITERAL(7, 166, 32), // "on_actionXfce_Terminal_triggered"
QT_MOC_LITERAL(8, 199, 24), // "on_actionAbout_triggered"
QT_MOC_LITERAL(9, 224, 23), // "on_actionHelp_triggered"
QT_MOC_LITERAL(10, 248, 5) // "other"

    },
    "nfancontrol\0on_horizontalSlider_sliderReleased\0"
    "\0on_spinBox_editingFinished\0"
    "on_actionGnome_Terminal_triggered\0"
    "on_actionTerminator_triggered\0"
    "on_actionKonsole_triggered\0"
    "on_actionXfce_Terminal_triggered\0"
    "on_actionAbout_triggered\0"
    "on_actionHelp_triggered\0other"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_nfancontrol[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x08 /* Private */,
       3,    0,   60,    2, 0x08 /* Private */,
       4,    0,   61,    2, 0x08 /* Private */,
       5,    0,   62,    2, 0x08 /* Private */,
       6,    0,   63,    2, 0x08 /* Private */,
       7,    0,   64,    2, 0x08 /* Private */,
       8,    0,   65,    2, 0x08 /* Private */,
       9,    0,   66,    2, 0x08 /* Private */,
      10,    0,   67,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void nfancontrol::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        nfancontrol *_t = static_cast<nfancontrol *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_horizontalSlider_sliderReleased(); break;
        case 1: _t->on_spinBox_editingFinished(); break;
        case 2: _t->on_actionGnome_Terminal_triggered(); break;
        case 3: _t->on_actionTerminator_triggered(); break;
        case 4: _t->on_actionKonsole_triggered(); break;
        case 5: _t->on_actionXfce_Terminal_triggered(); break;
        case 6: _t->on_actionAbout_triggered(); break;
        case 7: _t->on_actionHelp_triggered(); break;
        case 8: _t->other(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject nfancontrol::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_nfancontrol.data,
      qt_meta_data_nfancontrol,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *nfancontrol::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *nfancontrol::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_nfancontrol.stringdata0))
        return static_cast<void*>(const_cast< nfancontrol*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int nfancontrol::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
