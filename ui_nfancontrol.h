/********************************************************************************
** Form generated from reading UI file 'nfancontrol.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NFANCONTROL_H
#define UI_NFANCONTROL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_nfancontrol
{
public:
    QAction *actionGnome_Terminal;
    QAction *actionTerminator;
    QAction *actionKonsole;
    QAction *actionXfce_Terminal;
    QAction *actionHelp;
    QAction *actionAbout;
    QWidget *centralWidget;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QSlider *horizontalSlider;
    QSpinBox *spinBox;
    QLabel *temperature;
    QFrame *frame;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QCheckBox *EnableFan_3;
    QCheckBox *EnableFan_4;
    QCheckBox *EnableFan_5;
    QSpinBox *manfan;
    QSpinBox *manfan2;
    QSpinBox *manfan3;
    QSpinBox *manfan4;
    QSpinBox *manfan5;
    QSpinBox *fanspeed;
    QSpinBox *fanspeed_2;
    QSpinBox *fanspeed_3;
    QSpinBox *fanspeed_4;
    QSpinBox *fanspeed_5;
    QLabel *label_4;
    QCheckBox *EnableManFan;
    QMenuBar *menuBar;
    QMenu *menuFirst_Run;
    QMenu *menuHelp;

    void setupUi(QMainWindow *nfancontrol)
    {
        if (nfancontrol->objectName().isEmpty())
            nfancontrol->setObjectName(QStringLiteral("nfancontrol"));
        nfancontrol->resize(200, 245);
        nfancontrol->setMinimumSize(QSize(200, 245));
        nfancontrol->setMaximumSize(QSize(200, 245));
        actionGnome_Terminal = new QAction(nfancontrol);
        actionGnome_Terminal->setObjectName(QStringLiteral("actionGnome_Terminal"));
        actionTerminator = new QAction(nfancontrol);
        actionTerminator->setObjectName(QStringLiteral("actionTerminator"));
        actionKonsole = new QAction(nfancontrol);
        actionKonsole->setObjectName(QStringLiteral("actionKonsole"));
        actionXfce_Terminal = new QAction(nfancontrol);
        actionXfce_Terminal->setObjectName(QStringLiteral("actionXfce_Terminal"));
        actionHelp = new QAction(nfancontrol);
        actionHelp->setObjectName(QStringLiteral("actionHelp"));
        actionAbout = new QAction(nfancontrol);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        centralWidget = new QWidget(nfancontrol);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 0, 201, 27));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSlider = new QSlider(layoutWidget);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setMaximumSize(QSize(75, 25));
        horizontalSlider->setMaximum(100);
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayout->addWidget(horizontalSlider);

        spinBox = new QSpinBox(layoutWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMaximumSize(QSize(28, 25));
        spinBox->setFrame(true);
        spinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox->setMinimum(1);
        spinBox->setMaximum(100);

        horizontalLayout->addWidget(spinBox);

        temperature = new QLabel(layoutWidget);
        temperature->setObjectName(QStringLiteral("temperature"));
        temperature->setMinimumSize(QSize(35, 24));
        temperature->setMaximumSize(QSize(35, 24));
        temperature->setFrameShape(QFrame::StyledPanel);
        temperature->setFrameShadow(QFrame::Sunken);
        temperature->setLineWidth(1);
        temperature->setMidLineWidth(1);
        temperature->setText(QStringLiteral(""));
        temperature->setTextFormat(Qt::RichText);
        temperature->setScaledContents(false);
        temperature->setAlignment(Qt::AlignCenter);
        temperature->setWordWrap(true);

        horizontalLayout->addWidget(temperature);

        frame = new QFrame(centralWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setEnabled(false);
        frame->setGeometry(QRect(0, 50, 201, 171));
        frame->setFrameShape(QFrame::Panel);
        frame->setFrameShadow(QFrame::Sunken);
        frame->setLineWidth(4);
        frame->setMidLineWidth(3);
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 60, 41, 17));
        label_2 = new QLabel(frame);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(81, 10, 41, 17));
        label_3 = new QLabel(frame);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(140, 10, 59, 17));
        EnableFan_3 = new QCheckBox(frame);
        EnableFan_3->setObjectName(QStringLiteral("EnableFan_3"));
        EnableFan_3->setEnabled(false);
        EnableFan_3->setGeometry(QRect(41, 81, 20, 20));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(20);
        sizePolicy.setVerticalStretch(20);
        sizePolicy.setHeightForWidth(EnableFan_3->sizePolicy().hasHeightForWidth());
        EnableFan_3->setSizePolicy(sizePolicy);
        EnableFan_3->setMinimumSize(QSize(20, 20));
        EnableFan_3->setMaximumSize(QSize(20, 20));
        EnableFan_4 = new QCheckBox(frame);
        EnableFan_4->setObjectName(QStringLiteral("EnableFan_4"));
        EnableFan_4->setEnabled(false);
        EnableFan_4->setGeometry(QRect(41, 107, 20, 20));
        sizePolicy.setHeightForWidth(EnableFan_4->sizePolicy().hasHeightForWidth());
        EnableFan_4->setSizePolicy(sizePolicy);
        EnableFan_4->setMinimumSize(QSize(20, 20));
        EnableFan_4->setMaximumSize(QSize(20, 20));
        EnableFan_5 = new QCheckBox(frame);
        EnableFan_5->setObjectName(QStringLiteral("EnableFan_5"));
        EnableFan_5->setEnabled(false);
        EnableFan_5->setGeometry(QRect(41, 133, 20, 20));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(20);
        sizePolicy1.setVerticalStretch(20);
        sizePolicy1.setHeightForWidth(EnableFan_5->sizePolicy().hasHeightForWidth());
        EnableFan_5->setSizePolicy(sizePolicy1);
        EnableFan_5->setMinimumSize(QSize(20, 20));
        EnableFan_5->setMaximumSize(QSize(20, 20));
        manfan = new QSpinBox(frame);
        manfan->setObjectName(QStringLiteral("manfan"));
        manfan->setEnabled(false);
        manfan->setGeometry(QRect(81, 35, 30, 20));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(30);
        sizePolicy2.setVerticalStretch(20);
        sizePolicy2.setHeightForWidth(manfan->sizePolicy().hasHeightForWidth());
        manfan->setSizePolicy(sizePolicy2);
        manfan->setMinimumSize(QSize(30, 20));
        manfan->setMaximumSize(QSize(30, 20));
        manfan->setFrame(true);
        manfan->setAlignment(Qt::AlignCenter);
        manfan->setButtonSymbols(QAbstractSpinBox::NoButtons);
        manfan->setSpecialValueText(QStringLiteral(""));
        manfan->setMinimum(1);
        manfan->setMaximum(100);
        manfan->setValue(45);
        manfan2 = new QSpinBox(frame);
        manfan2->setObjectName(QStringLiteral("manfan2"));
        manfan2->setEnabled(false);
        manfan2->setGeometry(QRect(81, 59, 30, 20));
        sizePolicy2.setHeightForWidth(manfan2->sizePolicy().hasHeightForWidth());
        manfan2->setSizePolicy(sizePolicy2);
        manfan2->setMinimumSize(QSize(30, 20));
        manfan2->setMaximumSize(QSize(30, 20));
        manfan2->setFrame(true);
        manfan2->setAlignment(Qt::AlignCenter);
        manfan2->setButtonSymbols(QAbstractSpinBox::NoButtons);
        manfan2->setSpecialValueText(QStringLiteral(""));
        manfan2->setMinimum(1);
        manfan2->setMaximum(100);
        manfan2->setSingleStep(0);
        manfan2->setValue(60);
        manfan3 = new QSpinBox(frame);
        manfan3->setObjectName(QStringLiteral("manfan3"));
        manfan3->setEnabled(false);
        manfan3->setGeometry(QRect(81, 83, 30, 20));
        sizePolicy2.setHeightForWidth(manfan3->sizePolicy().hasHeightForWidth());
        manfan3->setSizePolicy(sizePolicy2);
        manfan3->setMinimumSize(QSize(30, 20));
        manfan3->setMaximumSize(QSize(30, 20));
        manfan3->setFrame(true);
        manfan3->setAlignment(Qt::AlignCenter);
        manfan3->setButtonSymbols(QAbstractSpinBox::NoButtons);
        manfan3->setSpecialValueText(QStringLiteral(""));
        manfan3->setMinimum(1);
        manfan3->setMaximum(100);
        manfan3->setValue(70);
        manfan4 = new QSpinBox(frame);
        manfan4->setObjectName(QStringLiteral("manfan4"));
        manfan4->setEnabled(false);
        manfan4->setGeometry(QRect(81, 107, 30, 20));
        sizePolicy2.setHeightForWidth(manfan4->sizePolicy().hasHeightForWidth());
        manfan4->setSizePolicy(sizePolicy2);
        manfan4->setMinimumSize(QSize(30, 20));
        manfan4->setMaximumSize(QSize(30, 20));
        manfan4->setFrame(true);
        manfan4->setAlignment(Qt::AlignCenter);
        manfan4->setButtonSymbols(QAbstractSpinBox::NoButtons);
        manfan4->setSpecialValueText(QStringLiteral(""));
        manfan4->setMinimum(1);
        manfan4->setMaximum(100);
        manfan4->setValue(90);
        manfan5 = new QSpinBox(frame);
        manfan5->setObjectName(QStringLiteral("manfan5"));
        manfan5->setEnabled(false);
        manfan5->setGeometry(QRect(81, 131, 30, 20));
        sizePolicy2.setHeightForWidth(manfan5->sizePolicy().hasHeightForWidth());
        manfan5->setSizePolicy(sizePolicy2);
        manfan5->setMinimumSize(QSize(30, 20));
        manfan5->setMaximumSize(QSize(30, 20));
        manfan5->setFrame(true);
        manfan5->setAlignment(Qt::AlignCenter);
        manfan5->setButtonSymbols(QAbstractSpinBox::NoButtons);
        manfan5->setSpecialValueText(QStringLiteral(""));
        manfan5->setMinimum(1);
        manfan5->setMaximum(100);
        manfan5->setValue(100);
        fanspeed = new QSpinBox(frame);
        fanspeed->setObjectName(QStringLiteral("fanspeed"));
        fanspeed->setEnabled(false);
        fanspeed->setGeometry(QRect(141, 35, 30, 20));
        sizePolicy2.setHeightForWidth(fanspeed->sizePolicy().hasHeightForWidth());
        fanspeed->setSizePolicy(sizePolicy2);
        fanspeed->setMinimumSize(QSize(30, 20));
        fanspeed->setMaximumSize(QSize(30, 20));
        fanspeed->setFrame(true);
        fanspeed->setAlignment(Qt::AlignCenter);
        fanspeed->setButtonSymbols(QAbstractSpinBox::NoButtons);
        fanspeed->setSpecialValueText(QStringLiteral(""));
        fanspeed->setMinimum(1);
        fanspeed->setMaximum(100);
        fanspeed->setValue(30);
        fanspeed_2 = new QSpinBox(frame);
        fanspeed_2->setObjectName(QStringLiteral("fanspeed_2"));
        fanspeed_2->setEnabled(false);
        fanspeed_2->setGeometry(QRect(141, 59, 30, 20));
        sizePolicy2.setHeightForWidth(fanspeed_2->sizePolicy().hasHeightForWidth());
        fanspeed_2->setSizePolicy(sizePolicy2);
        fanspeed_2->setMinimumSize(QSize(30, 20));
        fanspeed_2->setMaximumSize(QSize(30, 20));
        fanspeed_2->setFrame(true);
        fanspeed_2->setAlignment(Qt::AlignCenter);
        fanspeed_2->setButtonSymbols(QAbstractSpinBox::NoButtons);
        fanspeed_2->setSpecialValueText(QStringLiteral(""));
        fanspeed_2->setMinimum(1);
        fanspeed_2->setMaximum(100);
        fanspeed_2->setValue(50);
        fanspeed_3 = new QSpinBox(frame);
        fanspeed_3->setObjectName(QStringLiteral("fanspeed_3"));
        fanspeed_3->setEnabled(false);
        fanspeed_3->setGeometry(QRect(141, 83, 30, 20));
        sizePolicy2.setHeightForWidth(fanspeed_3->sizePolicy().hasHeightForWidth());
        fanspeed_3->setSizePolicy(sizePolicy2);
        fanspeed_3->setMinimumSize(QSize(30, 20));
        fanspeed_3->setMaximumSize(QSize(30, 20));
        fanspeed_3->setFrame(true);
        fanspeed_3->setAlignment(Qt::AlignCenter);
        fanspeed_3->setButtonSymbols(QAbstractSpinBox::NoButtons);
        fanspeed_3->setSpecialValueText(QStringLiteral(""));
        fanspeed_3->setMinimum(1);
        fanspeed_3->setMaximum(100);
        fanspeed_3->setValue(80);
        fanspeed_4 = new QSpinBox(frame);
        fanspeed_4->setObjectName(QStringLiteral("fanspeed_4"));
        fanspeed_4->setEnabled(false);
        fanspeed_4->setGeometry(QRect(141, 107, 30, 20));
        sizePolicy2.setHeightForWidth(fanspeed_4->sizePolicy().hasHeightForWidth());
        fanspeed_4->setSizePolicy(sizePolicy2);
        fanspeed_4->setMinimumSize(QSize(30, 20));
        fanspeed_4->setMaximumSize(QSize(30, 20));
        fanspeed_4->setFrame(true);
        fanspeed_4->setAlignment(Qt::AlignCenter);
        fanspeed_4->setButtonSymbols(QAbstractSpinBox::NoButtons);
        fanspeed_4->setSpecialValueText(QStringLiteral(""));
        fanspeed_4->setMinimum(1);
        fanspeed_4->setMaximum(100);
        fanspeed_4->setValue(90);
        fanspeed_5 = new QSpinBox(frame);
        fanspeed_5->setObjectName(QStringLiteral("fanspeed_5"));
        fanspeed_5->setEnabled(false);
        fanspeed_5->setGeometry(QRect(141, 131, 30, 20));
        sizePolicy2.setHeightForWidth(fanspeed_5->sizePolicy().hasHeightForWidth());
        fanspeed_5->setSizePolicy(sizePolicy2);
        fanspeed_5->setMinimumSize(QSize(30, 20));
        fanspeed_5->setMaximumSize(QSize(30, 20));
        fanspeed_5->setFrame(true);
        fanspeed_5->setAlignment(Qt::AlignCenter);
        fanspeed_5->setButtonSymbols(QAbstractSpinBox::NoButtons);
        fanspeed_5->setSpecialValueText(QStringLiteral(""));
        fanspeed_5->setMinimum(1);
        fanspeed_5->setMaximum(100);
        fanspeed_5->setValue(100);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(60, 30, 101, 17));
        label_4->setText(QStringLiteral("Auto Fan Control"));
        EnableManFan = new QCheckBox(centralWidget);
        EnableManFan->setObjectName(QStringLiteral("EnableManFan"));
        EnableManFan->setEnabled(true);
        EnableManFan->setGeometry(QRect(30, 30, 20, 20));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(16);
        sizePolicy3.setVerticalStretch(16);
        sizePolicy3.setHeightForWidth(EnableManFan->sizePolicy().hasHeightForWidth());
        EnableManFan->setSizePolicy(sizePolicy3);
        EnableManFan->setLayoutDirection(Qt::LeftToRight);
        EnableManFan->setAutoFillBackground(false);
        EnableManFan->setStyleSheet(QStringLiteral(""));
        EnableManFan->setAutoRepeat(false);
        EnableManFan->setAutoExclusive(false);
        nfancontrol->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(nfancontrol);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 200, 22));
        menuFirst_Run = new QMenu(menuBar);
        menuFirst_Run->setObjectName(QStringLiteral("menuFirst_Run"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        nfancontrol->setMenuBar(menuBar);

        menuBar->addAction(menuFirst_Run->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFirst_Run->addAction(actionGnome_Terminal);
        menuFirst_Run->addAction(actionTerminator);
        menuFirst_Run->addAction(actionKonsole);
        menuFirst_Run->addAction(actionXfce_Terminal);
        menuHelp->addAction(actionHelp);
        menuHelp->addAction(actionAbout);

        retranslateUi(nfancontrol);
        QObject::connect(spinBox, SIGNAL(valueChanged(int)), horizontalSlider, SLOT(setValue(int)));
        QObject::connect(horizontalSlider, SIGNAL(valueChanged(int)), spinBox, SLOT(setValue(int)));
        QObject::connect(EnableFan_5, SIGNAL(toggled(bool)), fanspeed_5, SLOT(setEnabled(bool)));
        QObject::connect(EnableFan_4, SIGNAL(toggled(bool)), fanspeed_4, SLOT(setEnabled(bool)));
        QObject::connect(EnableFan_5, SIGNAL(toggled(bool)), manfan5, SLOT(setEnabled(bool)));
        QObject::connect(EnableFan_3, SIGNAL(toggled(bool)), fanspeed_3, SLOT(setEnabled(bool)));
        QObject::connect(EnableFan_4, SIGNAL(toggled(bool)), manfan4, SLOT(setEnabled(bool)));
        QObject::connect(EnableFan_3, SIGNAL(toggled(bool)), manfan3, SLOT(setEnabled(bool)));
        QObject::connect(EnableManFan, SIGNAL(toggled(bool)), frame, SLOT(setEnabled(bool)));
        QObject::connect(EnableManFan, SIGNAL(toggled(bool)), horizontalSlider, SLOT(setDisabled(bool)));
        QObject::connect(EnableManFan, SIGNAL(toggled(bool)), spinBox, SLOT(setDisabled(bool)));
        QObject::connect(EnableManFan, SIGNAL(toggled(bool)), EnableFan_3, SLOT(setEnabled(bool)));
        QObject::connect(EnableManFan, SIGNAL(toggled(bool)), manfan, SLOT(setEnabled(bool)));
        QObject::connect(EnableManFan, SIGNAL(toggled(bool)), fanspeed, SLOT(setEnabled(bool)));
        QObject::connect(EnableManFan, SIGNAL(toggled(bool)), manfan2, SLOT(setEnabled(bool)));
        QObject::connect(EnableManFan, SIGNAL(toggled(bool)), fanspeed_2, SLOT(setEnabled(bool)));

        QMetaObject::connectSlotsByName(nfancontrol);
    } // setupUi

    void retranslateUi(QMainWindow *nfancontrol)
    {
        nfancontrol->setWindowTitle(QApplication::translate("nfancontrol", "n Fan Control", 0));
        actionGnome_Terminal->setText(QApplication::translate("nfancontrol", "Gnome Terminal", 0));
        actionTerminator->setText(QApplication::translate("nfancontrol", "Terminator", 0));
        actionKonsole->setText(QApplication::translate("nfancontrol", "Konsole", 0));
        actionXfce_Terminal->setText(QApplication::translate("nfancontrol", "Xfce Terminal", 0));
        actionHelp->setText(QApplication::translate("nfancontrol", "Help", 0));
        actionAbout->setText(QApplication::translate("nfancontrol", "About", 0));
        label->setText(QApplication::translate("nfancontrol", "Enable", 0));
        label_2->setText(QApplication::translate("nfancontrol", "Temp", 0));
        label_3->setText(QApplication::translate("nfancontrol", "Fan", 0));
        EnableFan_3->setText(QString());
        EnableFan_4->setText(QString());
        EnableFan_5->setText(QString());
        EnableManFan->setText(QString());
        menuFirst_Run->setTitle(QApplication::translate("nfancontrol", "First Run", 0));
        menuHelp->setTitle(QApplication::translate("nfancontrol", "Help", 0));
    } // retranslateUi

};

namespace Ui {
    class nfancontrol: public Ui_nfancontrol {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NFANCONTROL_H
